<?php

namespace PanelSsh\Shared\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use PanelSsh\Core\Traits\NanoidTrait;
use PanelSsh\Core\Traits\StatusTrait;

/**
 * @property int                $id
 * @property string             $slug
 * @property string             $name
 * @property string             $description
 * @property string             $command
 * @property string             $command_create
 * @property string             $command_renew
 * @property string             $command_change_password
 * @property array|object|null  $services
 * @property array|object|null  $properties
 * @property bool               $is_active
 * @property Carbon|string|null $created_at
 * @property array|object|null  $created_by
 * @property Carbon|string|null $updated_at
 * @property array|object|null  $updated_by
 * @property Collection         $servers
 */
class ServerType extends Model
{
    use NanoidTrait;
    use StatusTrait;

    protected $table = 'server_type';

    protected $fillable = [
        'id_ext',
        'slug',
        'name',
        'description',
        'command_create',
        'command_renew',
        'command_change_password',
        'command_delete',
        'services',
        'properties',
        'is_active',
        'created_by',
        'updated_by',
    ];

    protected $casts = [
        'is_active' => 'bool',
        'services' => 'array',
        'properties' => 'array',
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
        'created_by' => 'array',
        'updated_by' => 'array',
    ];

    public function servers()
    {
        return $this->hasMany(Server::class, 'type_id');
    }

    public function setServicesAttribute($value)
    {
        if (isset($value)) {
            $values = collect(array_values($value))->map(function ($item) {
                return [
                    'id' => $this->id_ext,
                    'name' => $item['name'],
                    'ports' => $item['ports'],
                    'restart_command' => $item['restart_command'],
                ];
            });

            $this->attributes['services'] = $this->asJson($values->toArray());
        }
    }

    public function setPropertiesAttribute($value)
    {
        if (isset($value)) {
            $values = collect(array_values($value))->map(function ($item) {
                return [
                    'id' => $this->id_ext,
                    'name' => $item['name'],
                    'description' => $item['description'],
                ];
            });

            $this->attributes['properties'] = $this->asJson($values->toArray());
        }
    }
}
