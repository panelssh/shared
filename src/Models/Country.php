<?php

namespace PanelSsh\Shared\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * @property int                $id
 * @property string             $id_ext
 * @property string             $slug
 * @property string             $name
 * @property string             $region
 * @property string             $sub_region
 * @property string             $flag
 * @property double             $latitude
 * @property double             $longitude
 * @property Carbon|string|null $created_at
 * @property array|object|null  $created_by
 * @property Carbon|string|null $updated_at
 * @property array|object|null  $updated_by
 */
class Country extends Model
{
    protected $table = 'm_country';

    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
        'created_by' => 'array',
        'updated_by' => 'array',
    ];
}
