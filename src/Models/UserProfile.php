<?php

namespace PanelSsh\Shared\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use PanelSsh\Shared\Database\Factories\UserProfileFactory;

/**
 * @property int                $id
 * @property UserAuth           $auth
 * @property string             $id_ext
 * @property string             $email
 * @property string             $first_name
 * @property string|null        $last_name
 * @property-read string        $full_name
 * @property string|null        $avatar_image
 * @property-read string        $avatar_image_url
 * @property Carbon|string|null $created_at
 * @property array|object|null  $created_by
 * @property Carbon|string|null $updated_at
 * @property array|object|null  $updated_by
 */
class UserProfile extends Model
{
    use HasFactory;

    const PATH_AVATAR_IMAGE = 'avatars';

    protected $table = 'user_profile';

    protected $fillable = [
        'id_ext',
        'email',
        'first_name',
        'last_name',
        'avatar_image',
        'created_by',
        'updated_by',
    ];

    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
        'created_by' => 'array',
        'updated_by' => 'array',
    ];

    protected static function newFactory()
    {
        return UserProfileFactory::new();
    }

    public function auth()
    {
        return $this->hasOne(UserAuth::class, 'id');
    }

    public function accounts()
    {
        return $this->hasMany(Account::class, 'user_id');
    }

    public function getFullNameAttribute()
    {
        return is_null($this->last_name) ? $this->first_name : $this->first_name . ' ' . $this->last_name;
    }

    public function getAvatarImageUrlAttribute()
    {
        return is_null($this->avatar_image) ? asset('images/default.png') : \Storage::url($this->avatar_image);
    }
}
