<?php

namespace PanelSsh\Shared\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use PanelSsh\Core\Traits\NanoidTrait;
use PanelSsh\Core\Traits\StatusTrait;
use PanelSsh\Shared\Database\Factories\AccountFactory;

/**
 * @property int                $id
 * @property string             $username
 * @property string             $password
 * @property UserAuth|null      $user
 * @property int|null           $user_id
 * @property int|null           $user_id_ext
 * @property int|null           $user_email
 * @property Server             $server
 * @property int                $server_id
 * @property int                $server_id_ext
 * @property string|null        $server_name
 * @property string|null        $server_slug
 * @property bool               $is_active
 * @property Carbon|string      $expired_at
 * @property Carbon|string|null $created_at
 * @property array|object       $created_by
 * @property Carbon|string|null $updated_at
 * @property array|object       $updated_by
 */
class Account extends Model
{
    use HasFactory;
    use NanoidTrait;
    use StatusTrait;

    protected $table = 'account';

    protected $fillable = [
        'id_ext',
        'username',
        'password',
        'user_id',
        'user_id_ext',
        'user_email',
        'server_id',
        'server_id_ext',
        'server_name',
        'server_slug',
        'is_active',
        'expired_at',
        'created_by',
        'updated_by',
    ];

    protected $hidden = [
        'password',
    ];

    protected $casts = [
        'is_active' => 'bool',
        'expired_at' => 'datetime:Y-m-d',
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
        'created_by' => 'array',
        'updated_by' => 'array',
    ];

    protected static function newFactory()
    {
        return AccountFactory::new();
    }

    public function user()
    {
        return $this->belongsTo(UserAuth::class, 'user_id');
    }

    public function server()
    {
        return $this->belongsTo(Server::class, 'server_id');
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = encrypt($value);
    }

    public function getPasswordAttribute($value)
    {
        return decrypt($value);
    }
}
