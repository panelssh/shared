<?php

namespace PanelSsh\Shared\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use PanelSsh\Core\Traits\NanoidTrait;
use PanelSsh\Core\Traits\StatusTrait;
use PanelSsh\Shared\Database\Factories\ServerFactory;

/**
 * @property int                $id
 * @property string             $id_ext
 * @property string             $slug
 * @property string             $name
 * @property string             $description
 * @property string             $ip_address
 * @property string             $hostname
 * @property string             $username
 * @property string             $password
 * @property int                $port
 * @property int                $status
 * @property ServerType         $type
 * @property int                $type_id
 * @property int                $type_id_ext
 * @property string             $type_name
 * @property string             $type_slug
 * @property Country            $country
 * @property int                $country_id
 * @property int                $country_id_ext
 * @property string             $country_name
 * @property string             $country_slug
 * @property int                $city_name
 * @property array|object|null  $services
 * @property array|object|null  $properties
 * @property bool               $is_active
 * @property Carbon|string|null $created_at
 * @property array|object|null  $created_by
 * @property Carbon|string|null $updated_at
 * @property array|object|null  $updated_by
 */
class Server extends Model
{
    use HasFactory;
    use NanoidTrait;
    use StatusTrait;

    const STATUS_UNKNOWN = 0;
    const STATUS_CONNECTED = 1;
    const STATUS_ERROR = 2;

    protected $table = 'server';

    protected $fillable = [
        'id_ext',
        'slug',
        'name',
        'description',
        'ip_address',
        'hostname',
        'username',
        'password',
        'port',
        'type_id',
        'type_id_ext',
        'type_name',
        'type_slug',
        'country_id',
        'country_id_ext',
        'country_name',
        'country_slug',
        'city_name',
        'services',
        'properties',
        'is_active',
        'created_by',
        'updated_by',
    ];

    protected $hidden = [
        'password',
    ];

    protected $casts = [
        'is_active' => 'bool',
        'services' => 'array',
        'properties' => 'array',
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
        'created_by' => 'array',
        'updated_by' => 'array',
    ];

    protected static function newFactory()
    {
        return ServerFactory::new();
    }

    public function type()
    {
        return $this->belongsTo(ServerType::class, 'type_id');
    }

    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }

    public function accounts()
    {
        return $this->hasMany(Account::class, 'server_id');
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = encrypt($value);
    }

    public function getPasswordAttribute($value)
    {
        return decrypt($value);
    }

    public function setServicesAttribute($value)
    {
        if (isset($value)) {
            $values = collect(array_values($value))->map(function ($item) {
                return [
                    'id' => $this->id_ext,
                    'name' => $item['name'],
                    'ports' => $item['ports'],
                    'restart_command' => $item['restart_command'],
                ];
            });

            $this->attributes['services'] = $this->asJson($values->toArray());
        }
    }

    public function setPropertiesAttribute($value)
    {
        if (isset($value)) {
            $values = collect(array_values($value))->map(function ($item) {
                return [
                    'id' => $this->id_ext,
                    'name' => $item['name'],
                    'description' => $item['description'],
                ];
            });

            $this->attributes['properties'] = $this->asJson($values->toArray());
        }
    }
}
