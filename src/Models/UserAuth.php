<?php

namespace PanelSsh\Shared\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Carbon;
use PanelSsh\Core\Traits\StatusTrait;
use PanelSsh\Shared\Database\Factories\UserAuthFactory;

/**
 * @property int                $id
 * @property UserProfile        $profile
 * @property string             $id_ext
 * @property string             $email
 * @property string             $password
 * @property bool               $is_active
 * @property Carbon|string|null $last_seen_at
 * @property Carbon|string|null $last_login_at
 * @property Carbon|string|null $email_verified_at
 * @property Carbon|string|null $created_at
 * @property array|object|null  $created_by
 * @property Carbon|string|null $updated_at
 * @property array|object|null  $updated_by
 */
class UserAuth extends Authenticatable implements MustVerifyEmail
{
    use HasFactory;
    use Notifiable;
    use StatusTrait;

    protected $table = 'user_auth';

    protected $with = ['profile'];

    protected $fillable = [
        'id_ext',
        'email',
        'password',
        'is_active',
        'last_seen_at',
        'last_login_at',
        'email_verified_at',
        'created_by',
        'updated_by',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'is_active' => 'bool',
        'is_verified' => 'bool',
        'last_seen_at' => 'datetime:Y-m-d H:i:s',
        'last_login_at' => 'datetime:Y-m-d H:i:s',
        'email_verified_at' => 'datetime:Y-m-d H:i:s',
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
        'created_by' => 'array',
        'updated_by' => 'array',
    ];

    protected static function newFactory()
    {
        return UserAuthFactory::new();
    }

    public function profile()
    {
        return $this->hasOne(UserProfile::class, 'id');
    }

    public function accounts()
    {
        return $this->hasMany(Account::class, 'user_id');
    }
}
