<?php

namespace PanelSsh\Shared\Database\Seeders;

use Illuminate\Database\Seeder;

class ServerTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('server_type')->insertOrIgnore([
            [
                'id' => 1,
                'id_ext' => 1,
                'slug' => 'ssh-server',
                'name' => 'SSH Server',
                'command_create' => 'useradd -e [expired_at] [username] -s /bin/false -M; { echo [password]; echo [password]; } | passwd [username];',
                'command_renew' => 'usermod -e [expired_at] [username];',
                'command_change_password' => '{ echo [password]; echo [password]; } | passwd [username];',
                'command_delete' => 'userdel -r [username];',
                'services' => json_encode([
                    [
                        'id' => 1,
                        'name' => 'SSH',
                        'ports' => '22',
                        'restart_command' => 'service ssh restart',
                    ],
                    [
                        'id' => 1,
                        'name' => 'Dropbear',
                        'ports' => '80,81',
                        'restart_command' => 'service dropbear restart',
                    ],
                    [
                        'id' => 1,
                        'name' => 'Stunnel4',
                        'ports' => '443,444',
                        'restart_command' => 'service stunnel4 restart',
                    ],
                ]),
                'is_active' => true,
                'created_at' => date('Y-m-d H:i:s'),
            ],
            [
                'id' => 2,
                'id_ext' => 2,
                'slug' => 'openvpn-server',
                'name' => 'OpenVPN Server',
                'command_create' => 'todo command [expired_at] [username] [password]',
                'command_renew' => 'todo command [expired_at] [username]',
                'command_change_password' => 'todo command [username] [password]',
                'command_delete' => 'todo command [username]',
                'services' => json_encode([
                    [
                        'id' => 2,
                        'name' => 'Squid3',
                        'ports' => '8000,8080,8888',
                        'restart_command' => 'service squid3 restart',
                    ],
                    [
                        'id' => 2,
                        'name' => 'Badvpn',
                        'ports' => '7300',
                        'restart_command' => 'screen -wipe && screen -AmdS badvpn badvpn-udpgw --listen-addr 127.0.0.1:7300',
                    ],
                ]),
                'is_active' => true,
                'created_at' => date('Y-m-d H:i:s'),
            ],
            [
                'id' => 3,
                'id_ext' => 3,
                'slug' => 'softether-server',
                'name' => 'SoftEther Server',
                'command_create' => 'todo command [expired_at] [username] [password]',
                'command_renew' => 'todo command [expired_at] [username]',
                'command_change_password' => 'todo command [username] [password]',
                'command_delete' => 'todo command [username]',
                'services' => json_encode([
                    [
                        'id' => 3,
                        'name' => 'badVPN',
                        'ports' => '7300',
                        'restart_command' => 'screen -wipe && screen -AmdS badvpn badvpn-udpgw --listen-addr 127.0.0.1:7300',
                    ],
                ]),
                'is_active' => true,
                'created_at' => date('Y-m-d H:i:s'),
            ],
        ]);
    }
}
