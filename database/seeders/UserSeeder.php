<?php

namespace PanelSsh\Shared\Database\Seeders;

use Illuminate\Database\Seeder;
use PanelSsh\Shared\Models\UserAuth;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UserAuth::factory(100)->create();
    }
}
