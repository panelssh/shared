<?php

namespace PanelSsh\Shared\Database\Seeders;

use Illuminate\Database\Seeder;
use PanelSsh\Shared\Models\Server;

class ServerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Server::factory(100)->create();
    }
}
