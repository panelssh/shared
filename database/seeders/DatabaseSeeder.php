<?php

namespace PanelSsh\Shared\Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UserSeeder::class,
            ServerTypeSeeder::class,
            ServerSeeder::class,
            AccountSeeder::class,
        ]);
    }
}
