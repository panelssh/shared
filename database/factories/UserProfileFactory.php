<?php

namespace PanelSsh\Shared\Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use PanelSsh\Shared\Models\UserProfile;

class UserProfileFactory extends Factory
{
    use HasFactory;

    protected $model = UserProfile::class;

    public function definition()
    {
        return [
            'id_ext' => nanoid(),
            'email' => $this->faker->unique()->safeEmail,
            'first_name' => $this->faker->firstName,
            'last_name' => $this->faker->lastName,
            'avatar_image' => $this->faker->imageUrl(200, 200),
            'created_at' => now(),
            'created_by' => [],
        ];
    }

    public function configure()
    {
        return $this->afterCreating(function (UserProfile $userProfile) {
            return $userProfile->auth()->create([
                'id_ext' => $userProfile->id_ext,
                'email' => $userProfile->email,
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
                'is_active' => $this->faker->boolean,
                'last_seen_at' => now(),
                'last_login_at' => now(),
                'email_verified_at' => now(),
                'created_at' => now(),
                'created_by' => [],
            ]);
        });
    }
}
