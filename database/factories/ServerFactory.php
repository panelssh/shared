<?php

namespace PanelSsh\Shared\Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use PanelSsh\Shared\Models\Country;
use PanelSsh\Shared\Models\Server;
use PanelSsh\Shared\Models\ServerType;

class ServerFactory extends Factory
{
    protected $model = Server::class;

    public function definition()
    {
        /** @var ServerType $serverType */
        $serverType = ServerType::active()->inRandomOrder()->first();

        /** @var Country $country */
        $country = Country::query()->inRandomOrder()->first();

        return [
            'slug' => $this->faker->slug,
            'name' => "{$country->flag} - {$country->name}",
            'ip_address' => $this->faker->ipv4,
            'username' => 'root',
            'password' => 'password',
            'port' => 22,
            'type_id' => $serverType->id,
            'type_id_ext' => $serverType->id_ext,
            'type_name' => $serverType->name,
            'type_slug' => $serverType->slug,
            'country_id' => $country->id,
            'country_id_ext' => $country->id_ext,
            'country_name' => $country->name,
            'country_slug' => $country->slug,
            'city_name' => $this->faker->city,
            'services' => $serverType->services,
            'properties' => collect(range(0, $this->faker->numberBetween(1, 5)))->map(function () {
                return [
                    'name' => $this->faker->words(2, true),
                    'description' => $this->faker->paragraph(1),
                ];
            })->toArray(),
            'is_active' => $this->faker->boolean,
        ];
    }
}
