<?php

namespace PanelSsh\Shared\Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;
use PanelSsh\Shared\Models\Account;
use PanelSsh\Shared\Models\Server;
use PanelSsh\Shared\Models\UserAuth;

class AccountFactory extends Factory
{
    protected $model = Account::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        /** @var UserAuth $user */
        $user = UserAuth::active()->inRandomOrder()->first();

        /** @var Server $server */
        $server = Server::active()->inRandomOrder()->first();

        return [
            'username' => $this->faker->userName,
            'password' => 'password',
            'user_id' => $user->id,
            'user_id_ext' => $user->id_ext,
            'user_email' => $user->email,
            'server_id' => $server->id,
            'server_id_ext' => $server->id_ext,
            'server_name' => $server->name,
            'server_slug' => $server->slug,
            'expired_at' => $expiredAt = $this->faker->dateTimeBetween('-1 years', '+1 years'),
            'created_at' => Carbon::parse($expiredAt)->subDays(3),
            'created_by' => [],
            'updated_at' => Carbon::parse($expiredAt)->subDays(1),
            'updated_by' => [],
            'is_active' => Carbon::parse($expiredAt)->gte(Carbon::now()),
        ];
    }
}
