<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServerTypeTable extends Migration
{
    public function up()
    {
        Schema::create('server_type', function (Blueprint $table) {
            $table->increments('id');
            $table->char('id_ext', 21)->unique();
            $table->string('slug')->unique();
            $table->string('name');
            $table->text('description')->nullable();
            $table->text('command_create');
            $table->text('command_renew');
            $table->text('command_change_password');
            $table->text('command_delete');
            $table->json('services')->nullable();
            $table->json('properties')->nullable();
            $table->boolean('is_active')->default(false);
            $table->timestamp('created_at')->nullable();
            $table->json('created_by')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->json('updated_by')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('server_type');
    }
}
